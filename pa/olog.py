import pickle


class Object():
    def __init__(self, name, desc=None):
        self.name = name
        self.desc = desc

    def as_tuple(self):
        return self.name, self.desc


class Relation():
    def __init__(self, name, ob1, ob2, desc=None):
        self.name = name
        self.desc = desc
        self.ob1 = ob1
        self.ob2 = ob2

    def as_tuple(self):
        return self.name, self.ob1.name, self.ob2.name, self.desc


class OLOG():
    def __init__(self):
        self.objects = {}
        self.relations = {}

    def save(self, path):
        with open(path, 'wb') as f:
            pickle.dump('objects', f)
            for ob in self.objects.values():
                pickle.dump(ob.as_tuple(), f)
            pickle.dump('relations', f)
            for rel in self.relations.values():
                pickle.dump(rel.as_tuple(), f)
            pickle.dump('end', f)

    def missed_section(self, title):
        raise ValueError('Bad pickle file: “{}”section header is missed.'.format(title))

    def read_objects(self, f, title):
        title == 'objects' or self.missed_section('objects')
        while True:
            item = pickle.load(f)
            if isinstance(item, str):
                return item
            self.newob_from_tuple(item)

    def read_relations(self, f, title):
        title == 'relations' or self.missed_section('relations')
        while True:
            item = pickle.load(f)
            if isinstance(item, str):
                return item
            self.newrel_from_tuple(item)

    def newob_from_tuple(self, arg):
        return self.newob(*arg)

    def newrel_from_tuple(self, arg):
        return self.newrel(*arg)

    def newob(self, name, desc=None):
        if name in self.objects.keys():
            raise ValueError('Object with name “{}” already exists.'.format(name))
        result = Object(name)
        self.objects[name] = result
        return result

    def newrel(self, name, ob1, ob2, desc=None):
        if name in self.relations.keys():
            raise ValueError('Relation with name “{}” already exists.'.format(name))
        ob1 = ob1.name if isinstance(ob1, Object) else str(ob1)
        ob2 = ob2.name if isinstance(ob2, Object) else str(ob2)
        if ob1 not in self.objects.keys():
            raise ValueError('Object with name “{}” is missed in OLOG.'.format(ob1))
        if ob2 not in self.objects.keys():
            raise ValueError('Object with name “{}” is missed in OLOG.'.format(ob2))
        result = Relation(name, self.objects[ob1], self.objects[ob2])
        self.relations[name] = result
        return result


def load(path):
    olog = OLOG()
    with open(path, 'rb') as f:
        title = pickle.load(f)
        title = olog.read_objects(f, title)
        title = olog.read_relations(f, title)
        title == 'end' or olog.missed_section('end')
    return olog
