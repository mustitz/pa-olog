#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from distutils.core import setup

setupArgs = {
    'name'         : 'パ OLOG',
    'version'      : '0.1',
    'description'  : 'Python library for working with OLOG (categorical knowladge representation).',
    'author'       : 'Andrii Sevastianov',
    'author_email' : 'mustitz@gmail.com',
    'license'      : 'MIT',
    'packages'     : ['pa']
}

setup(**setupArgs)
